package com.syncwords.alexandr.basic;

import static com.syncwords.alexandr.basic.OperatorsUtils.*;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class OperatorUtilsTest {
    
      @Test public void 
    testgetMinValueMethod()
    {
        float[][] testData = {
            {2, 9},
            {5, 3},
            {10, -6}
        };
        
        float[] expectedResult = {2, 3, -6};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            float actualOutput = getMinValue (testData[i][0], testData[i][1]);
            
            Assert.assertEquals (expectedResult[i], actualOutput, 0.0001);
        }
        
    }
    
      @Test public void 
    testGetAbsoluteValueMethod()
    {
        int[] testData = {2, -6, 0, -0};

        int[] expectedResult = {2, 6, 0, 0};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            int actualOutput = getAbsoluteValue (testData[i]);
            
            Assert.assertEquals (expectedResult[i], actualOutput);
        }        
    }
    
      @Test public void 
    testInvertBooleanMethod()
    {
        boolean[] testData = {true, false};

        boolean[] expectedResult = {false, true};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            boolean actualOutput = invertBoolean(testData[i]);
            
            Assert.assertEquals (expectedResult[i], actualOutput);
        }        
    }

        @Test public void 
    testIsContainStringMethod()
    {
        String[] testData = {"Alex", "qwerty", "Hi", "hi"};

        boolean[] expectedResult = {true, false, true, false};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            boolean actualOutput = isContainString(testData[i]);
            
            Assert.assertEquals (expectedResult[i], actualOutput);
        }        
    }
    
            @Test public void 
    testMyCustomLogMethod()
    {
        int [][] testData = {{2, 8}, {3, 243}, {10, 1000}};

        double[] expectedResult = {3, 5, 3};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            double actualOutput = customLog (testData[i][0], testData[i][1]);
            
            Assert.assertEquals (expectedResult[i], actualOutput, 0.0001);
        }        
    }
}

package com.syncwords.alexandr.basic;

import com.syncwords.alexandr.basic.SortMethods;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class SortMethodsTest {
    
      @Test public void 
    testBubbleSortMethod() {
        
        int[] inputData = {10, 5, -6, 0, 4, 3};
        
        int[] expectedResult = {-6, 0, 3, 4, 5, 10};
        
        int[] actualResult = SortMethods.bubbleSort(inputData);
        
        Assert.assertArrayEquals(expectedResult, actualResult);
    }
}

package com.syncwords.alexandr.basic;

import static com.syncwords.alexandr.basic.MatrixOperations.matrixMultiplication;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class MatrixOperationsTest {
    
    
      @Test public void 
    testMatrix() {
    
        int[][] firstMatrix = {
            {0, 1, 2, 3},
            {4, 8, 2, 3},
            {5, 1, 2, 3},
            {6, 1, 5, 3},
            {7, 1, 2, 1}
        };

        int[][] secondMatrix = {
            {0, 3}, 
            {2, 2}, 
            {4, 6},
            {6, 3}
        };

        int[][] expectedResult = {
            {28, 23}, 
            {42, 49}, 
            {28, 38}, 
            {40, 59}, 
            {16, 38} 
        };

        //Number of rows first matrix has been equals with number of columns second matrix 
        int actualResult [][] = matrixMultiplication (firstMatrix, secondMatrix);

        for(int i = 0; i < expectedResult.length; i++)
        {
            Assert.assertArrayEquals (expectedResult[i], actualResult[i]);
        }
    }
}

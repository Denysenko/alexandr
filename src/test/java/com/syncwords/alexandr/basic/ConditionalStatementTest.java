package com.syncwords.alexandr.basic;

import static com.syncwords.alexandr.basic.ConditionalStatement.*;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class ConditionalStatementTest {
    
      @Test public void 
    testDefinePosOrNegNumber() {
        
        int[] testData = {-5, 0, 5};
        
        String[] expectedResultValues = {"The number is negative: " + testData[0], "Zero number", "The number is positive: " + testData[2]};

        for (int i = 0; i < expectedResultValues.length; i++) 
        {
        
            String actualResult = definePosOrNegNumber(testData[i]);
        
            Assert.assertEquals(expectedResultValues[i], actualResult);
        }
    }
    
}

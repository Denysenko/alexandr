package com.syncwords.alexandr.basic;

import static com.syncwords.alexandr.basic.SwitchCase.*;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class SwitchCaseTest {
    
      @Test public void 
    testSwitchCaseDefinition() {
        
        int [] testData = {0, 5, 3, 10};

        String[] expectedResult = {"Default number", "5", "Default number", "10"};
        
        for (int i = 0; i < expectedResult.length; i++) 
        {
            String actualOutput = switchCaseDefinition (testData[i]);
            
            Assert.assertEquals (expectedResult[i], actualOutput);
        }       
    }
}

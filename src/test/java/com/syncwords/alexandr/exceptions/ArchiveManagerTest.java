package com.syncwords.alexandr.exceptions;

import static com.syncwords.alexandr.exceptions.ArchiveManager.getFreeLoginFromArchive;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class ArchiveManagerTest {
    
      @Test public void 
    testIfLoginNotAvailable () {

            Boolean actualResult = true;
            try 
            {
                getFreeLoginFromArchive("Alex");
            }
            catch (ArchiveLoginNotFoundException ex)
            {
                actualResult = false;
                ex.getExceptionMessage();
            }
            assertFalse(actualResult);

        }
    
      @Test public void 
    testIfLoginAvailable () {

            Boolean actualResult = true;
            try 
            {
                getFreeLoginFromArchive("Alexan");
            }
            catch (ArchiveLoginNotFoundException ex)
            {
                actualResult = false;
                ex.getExceptionMessage();
            }
            assertTrue(actualResult);

        }
}

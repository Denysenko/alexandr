package com.syncwords.alexandr.generics;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class generics {
    
      @Test public void 
    testGenericPairMap() {
        
            String expectedStringResult = "string value";
            Object expectedObjectResult = new Object();
        
            Pair pair = new Pair();
            
            pair.save("str", expectedStringResult);
            pair.save(1234, expectedObjectResult);
            
            String actualStringResult = (String) pair.getValueByKey("str");
            Object actualObjectResult = pair.getValueByKey(1234);
            
            Assert.assertEquals(expectedStringResult, actualStringResult);
            
            Assert.assertEquals(expectedObjectResult, actualObjectResult);
    
        }
    
}

package com.syncwords.alexandr.regex;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class NameValidationTest {
    
    
    
      @Test public void 
    testValidateMethodOnSumbols() {

        
        
            final boolean[] expectedResult = {true, false, false};
            String[] inputStrings = {"_sasha1-deny", ".sasha--", "sasha.denysenko"};
            
            for (int i = 0; i < expectedResult.length; i++) {
                
                boolean actualResult = NameValidation.validate(inputStrings[i]);
                
                Assert.assertEquals(expectedResult[i], actualResult);
            }
        }
    
          @Test public void 
    testValidateMethodOnSumbolsLength() {

            final boolean[] expectedResult = {true, false, false};
            String[] inputStrings = {"denysenko", "sashasdenysenkosashas", "sa"};
            
            for (int i = 0; i < expectedResult.length; i++) {
                
                boolean actualResult = NameValidation.validate(inputStrings[i]);
                
                Assert.assertEquals(expectedResult[i], actualResult);
            }
        }
}

package com.syncwords.alexandr.regex;

import org.junit.Test;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import static org.junit.Assert.assertTrue;

import static com.syncwords.alexandr.regex.Regex.*;

/**
 *
 * @author alexandr
 */

public class RegexTest {
    
      @Test public void 
    testRegexUtils() {
        
            Pattern pattern = Pattern.compile(START_BOUNDARY + between(3, 15, charclassUnion(LETTER, DIGIT, charclass('_', '-'))) + END_BOUNDARY);

            Matcher matcher = pattern.matcher("denysenko_al13");

            assertTrue(matcher.find());
        }
    
}

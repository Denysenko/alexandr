package com.syncwords.alexandr.oop.management;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class StaffMemberTest {
    
    
    ArrayList <String> expectedData;
    ArrayList <StaffMember> members;
            
      @Before public void
    initTestData() {
        
            expectedData = new ArrayList <> ();
            
            expectedData.add ("FreelancerBob1 got 875.0 y.e. for 350.0 hours"); 
            expectedData.add ("Boss got 540 y.e.");
            expectedData.add ("Freelancer2 got 75.0 y.e. for 50.0 hours");
            expectedData.add ("Volonter1 thanks for working!");
            expectedData.add ("Employee1 got 500 y.e.");

            members = new ArrayList <StaffMember> ();

            members.add (new Hourly ("FreelancerBob1", "kiev", "+380735849758", new BigDecimal (2.5)));
            members.add (new Executive ("Boss", "kiev", "+380638529504", 4637017, new BigDecimal (500)));
            members.add (new Hourly ("Freelancer2", "rovno", "+380669485729", new BigDecimal (1.5)));
            members.add (new Volounter ("Volonter1", "dnepr", "+380668361957"));
            members.add (new Employee ("Employee1", "kiev", "+380673856466", 6653465, new BigDecimal (500)));
            
            ((Hourly) members.get (0)).addHours (350);

            ((Hourly) members.get (2)).addHours (50);

            ((Executive) members.get (1)).chargeBonus (30);

            ((Executive) members.get (1)).chargeBonus (10);
        }
    
    
      @Test public void 
    testPayMethodsForAll () {

            for (int i = 0; i < members.size(); i++)
            {
                if (members.get (i) instanceof Employee)
                {
                    Assert.assertEquals (expectedData.get (i), ((Employee) members.get (i)).pay());
                } 
                else if (members.get (i) instanceof Hourly)
                {
                    Assert.assertEquals (expectedData.get (i), ((Hourly) members.get (i)).pay());
                } 
                else if (members.get (i) instanceof Executive)
                {
                    Assert.assertEquals (expectedData.get (i), ((Executive) members.get (i)).pay());
                } 
                else if (members.get (i) instanceof Volounter)
                {
                    Assert.assertEquals (expectedData.get (i), ((Volounter) members.get (i)).pay());
                } 
            }
        }
}

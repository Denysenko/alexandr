package com.syncwords.alexandr.cache;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class CacheTest {
    
      @Test public void 
    testHashCodeCompare() {

            Cache cache = new Cache();
            Object[] objects = {new Object(), new Object()};
            int[] expectedHashCodes = {objects[0].hashCode(), objects[1].hashCode()};

            cache.save("0", objects[0]);
            
            cache.save("45", new Object());
            
            cache.save("1", objects[1]);
            
            cache.remove("45");
            
            for(int i = 0; i < expectedHashCodes.length; i++) 
            {
                int actualHashCode = cache.getObject(String.valueOf(i)).hashCode();
            
                Assert.assertEquals(expectedHashCodes[i], actualHashCode);
            }
        }
    
}

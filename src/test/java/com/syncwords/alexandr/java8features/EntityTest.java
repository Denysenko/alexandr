package com.syncwords.alexandr.java8features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class EntityTest {
    
    List<Entity> list = Arrays.asList(
                    new Entity(1, "Bob"),
                    new Entity(2, "Tiny"),
                    new Entity(3, "Tom"),
                    new Entity(4, "Bred"),
                    new Entity(5, "Morty"));
    
    List<Entity> finalList = new ArrayList<>();
             
     @Test public void 
    testStreamMethods() {
        
            IntStream.range(0, list.size())
                    .filter((id) -> id == 0 || id == 2)
                    .forEach((id) -> { 
                        list.get(id).setId(0); 
                    });
            
            assertEquals(0, list.get(0).getId());
            assertEquals(0, list.get(2).getId());

            list.stream()
                    .collect(Collectors.groupingBy(Entity::getId))
                    .entrySet()
                    .stream()
                    .filter(t -> t.getValue().size() == 1)
                    .forEach((t) -> finalList.add(t.getValue().get(0)));
            
            assertTrue(finalList.size() == list.size() - 2);
            
            assertEquals(list.get(1), finalList.get(0));
            
            assertEquals(list.get(3), finalList.get(1));
            
            assertEquals(list.get(4), finalList.get(2));            
        }
}

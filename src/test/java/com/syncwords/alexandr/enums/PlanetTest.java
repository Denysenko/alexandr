package com.syncwords.alexandr.enums;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class PlanetTest {
    
      @Test public void 
    testEnumStates() {

            int expectedMarsPositionFromTheSun = 4;
            int expectedEarthPositionFromTheSun = 3;

            int actualMarsPositionFtomTheSun = Planet.MARS.getPlanetPositionToTheSun();
            int actualEarthPositionFtomTheSun = Planet.EARTH.getPlanetPositionToTheSun();
            
            Assert.assertEquals(expectedMarsPositionFromTheSun, actualMarsPositionFtomTheSun);
            Assert.assertEquals(expectedEarthPositionFromTheSun, actualEarthPositionFtomTheSun);

        }
}

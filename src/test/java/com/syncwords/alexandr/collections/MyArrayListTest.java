package com.syncwords.alexandr.collections;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class MyArrayListTest {
    
    MyList<Integer> listInteger = new MyArrayList<>();
    
    MyIterator<Integer> iterator = listInteger.iterator();
    
      @Test public void 
    testAddMethod() {
        
            listInteger.add (5);
            listInteger.add (4);
            listInteger.add (67);
            listInteger.add (15);

            assertTrue (listInteger.get (2) == 67);
        }
    
      @Test public void 
    testDinamicIncreasedList() {
        
        int numberElements = 50;
        
            for (int i = 0; i < numberElements; i++) {
                
                listInteger.add (i);
            }
            
            assertTrue (listInteger.size() >= numberElements);
        }
    
      @Test public void 
    testRemoveElement() {
        
            listInteger.add (0);
            listInteger.add (1);
            listInteger.add (2);
            listInteger.add (3);
            
            listInteger.remove (2);
            
            assertFalse (listInteger.get (2) == 2);
            
            for (int i = 0; i < 10; i++) {
                
                listInteger.remove (2);
            }
            
            assertTrue(listInteger.size() == 2); // test is try to remove last item in list
                        
        }
    
      @Test public void 
    testGetListElement() {
        
            listInteger.add (4);
            listInteger.add (8);
            
            assertTrue (listInteger.get (1) == 8);
            assertTrue (listInteger.get (2) == null);
            assertTrue (listInteger.get (100) == null);
        }
    
      @Test public void
    testIteratorHasNextAndNextMethods() {
        
            listInteger.add (10);
            listInteger.add (20);
            
            while (iterator.hasNext()) {
                
                assertTrue (iterator.next() != null);
            }
            
            assertFalse (iterator.hasNext());
        }
    
      @Test public void
    testIteratorRemoveMethod() {
        
            int numberElements = 10;
        
            for (int i = 0; i < numberElements; i++) {
            
                listInteger.add (i);
            }
            
            for(int i = 0; i < 4; i++) {
                
                iterator.next();
            }
            
            iterator.remove();

            assertFalse (listInteger.get (4) == 4);
            
            assertTrue (listInteger.size() == numberElements - 1);
        }
    
      @Test public void 
    testStringList() {
        
            MyList<String> stringsList = new MyArrayList<>();
            
            stringsList.add ("sdfjgiy12u3");
            stringsList.add (String.valueOf (23));
            
            stringsList.remove (0);
            
            assertTrue (stringsList.get (0).equals ("23"));
            assertTrue (stringsList.get (1) == null);
            
            //stringsList.add(new Object()); // incompatible types
        }
}

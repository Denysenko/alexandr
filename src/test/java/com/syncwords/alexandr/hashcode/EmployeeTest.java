package com.syncwords.alexandr.hashcode;


import java.util.Date;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 *
 * @author alexandr
 */
public class EmployeeTest {
    
    Employee empl1 = new Employee (1, "Bob", 500, 32, null);
    Employee empl2 = new Employee (1, "Bob", 500, 32, null);
    Employee empl3 = new Employee (1, "Frank", 500, 32, new Date());

    
      @Test public void 
    testEqualsFirstAndSecond() {

            assertTrue (empl1.equals(empl2));
        }
    
      @Test public void 
    testEqualsFirstAndThird() {

            assertFalse (empl1.equals(empl3));
        }
    
      @Test public void 
    testCompareHashCodeFirstAndSecond() {

            assertTrue (empl1.hashCode() == empl2.hashCode());
        }
    
      @Test public void 
    testCompareHashCodeFirstAndThird() {

            assertFalse (empl1.hashCode() == empl3.hashCode());
        }
    
      @Test public void 
    testNull() {
        
            assertFalse (empl1 .equals (null));
        }
    
      @Test public void 
    testWrong() {

            assertFalse (empl1 .equals (new Object()));
        }
    
      @Test public void
    testAllFields() {
            
            for (int i = 0 ; i < 5; i++) {
                
                Employee empl = new Employee(
                        i == 0 ? 2 : 1,
                        i == 1 ? "Mark" : "Frank",
                        i == 2 ? 300 : 500,
                        i == 3 ? 16 : 32,
                        i == 4 ? null : new Date()
                );
                
                assertFalse(empl3.equals(empl));
            }
        }
}

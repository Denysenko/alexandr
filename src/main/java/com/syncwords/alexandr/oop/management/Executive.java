package com.syncwords.alexandr.oop.management;

import java.math.BigDecimal;

/**
 *
 * @author alexandr
 */
public class Executive extends Employee {
    
            
      public 
    Executive(String name, String address, String phone, int socialSecurityNumber, BigDecimal payRate) {
            super(name, address, phone, socialSecurityNumber, payRate);
        }
    
      public void 
    chargeBonus (float suma) {
            this.payRate = this.payRate.add(new BigDecimal(suma));
            System.out.println(name + " got a bonus " + suma + " y.e.");     
        }
}

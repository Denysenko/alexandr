package com.syncwords.alexandr.oop.management;

/**
 *
 * @author alexandr
 */
public class Volounter extends StaffMember {

      public 
    Volounter(String name, String address, String phone) {
        
            super(name, address, phone);
        }

      @Override public String 
    pay() {
    
            return name + " thanks for working!";
      }
}

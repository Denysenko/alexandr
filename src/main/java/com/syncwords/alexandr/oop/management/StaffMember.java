package com.syncwords.alexandr.oop.management;

/**
 *
 * @author alexandr
 */
public abstract class StaffMember {
    
    protected String name;
    protected String address;
    protected String phone;

      public 
    StaffMember (String name, String address, String phone) {
        
            this.name = name;
            this.address = address;
            this.phone = phone;
        }

      public String 
    getName() {

            return name;
        }
    
      public String 
    getAddress() {
        
            return address;
        }

      public String 
    getPhone() {
        
            return phone;
        }

    public abstract String pay();
}

package com.syncwords.alexandr.oop.management;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author alexandr
 */
public class Employee extends StaffMember {
    
    protected int socialSecurityNumber;
    protected BigDecimal payRate;

      public 
    Employee (String name, String address, String phone, int socialSecurityNumber, BigDecimal payRate) {
        
            super (name, address, phone);
            this.socialSecurityNumber = socialSecurityNumber;
            this.payRate = payRate;
        }
    
      public BigDecimal 
    getPayRate() {
        
            return payRate;
        }
    
        public void 
    addPayRate(BigDecimal addPayRate) {
        
            payRate = payRate.add (addPayRate);
        }
    
      @Override public String 
    pay() {

            BigDecimal oldRate = this.payRate;
            this.payRate = new BigDecimal (BigInteger.ZERO);

            return name + " got " + oldRate + " y.e.";
        }
    
    
}

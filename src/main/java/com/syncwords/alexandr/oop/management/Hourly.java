package com.syncwords.alexandr.oop.management;

import java.math.BigDecimal;

/**
 *
 * @author alexandr
 */
public class Hourly extends StaffMember {
    
    private float hoursWorked;
    private BigDecimal moneyForHours;

      public 
    Hourly (String name, String address, String phone, BigDecimal moneyForHours) {
        
            super(name, address, phone);
            this.hoursWorked = 0;
            this.moneyForHours = moneyForHours;
        }

      public void 
    addHours(float hour) {
        
            hoursWorked += hour;
            System.out.println(name + " total hours working: " + hoursWorked + " hours.");
        }

      @Override public String 
    pay() {

            float oldHousWorked = hoursWorked;
            hoursWorked = 0;
            return name + " got " + moneyForHours.floatValue() * oldHousWorked + " y.e. for " + oldHousWorked + " hours";
        }
}

package com.syncwords.alexandr.oop.remotecontrol;

/**
 *
 * @author alex
 */
public interface Regulator {
    
    void increase();
    void reduce();
}

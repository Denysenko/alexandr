package com.syncwords.alexandr.oop.remotecontrol;

/**
 *
 * @author alexandr
 */
public interface RemoveControl {
    
    void openConnection();
    void closeConnection();
    
}

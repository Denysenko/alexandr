package com.syncwords.alexandr.oop.remotecontrol;

/**
 *
 * @author alexandr
 */
public interface SwitchButton {
    
    void on();
    void off();
}

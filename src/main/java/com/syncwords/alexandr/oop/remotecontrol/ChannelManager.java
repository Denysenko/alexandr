package com.syncwords.alexandr.oop.remotecontrol;

/**
 *
 * @author alexandr
 */
public interface ChannelManager {
    
    void nextChannel();
    void prevChannel();
}

package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class SedanToyota extends Toyota {
    
      @Override public String 
    toString() {
        
            return "Created sedan toyota";
        }
}

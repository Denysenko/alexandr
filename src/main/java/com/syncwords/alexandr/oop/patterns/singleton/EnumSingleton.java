package com.syncwords.alexandr.oop.patterns.singleton;

/**
 *
 * @author alexandr
 */
public enum EnumSingleton {
    
    INSTANCE;
}

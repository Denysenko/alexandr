package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class Main {
    
    public static void main(String[] args) {
        
        CarFactory carFactory = CarFactory.getFactory(BodyType.SEDAN);
        
        Car car = carFactory.createAudi();
        
        System.out.println(car);
    }
}

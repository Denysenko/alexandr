package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public class RegularExtra extends Extra {
    
      public 
    RegularExtra (Order order, double price, String label) {
        
            super(order, price, label);
        }
 
      @Override public double 
    getPrice() {
        
            return order.getPrice() + price;
        }
}

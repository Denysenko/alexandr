package com.syncwords.alexandr.oop.patterns.singleton;

/**
 *
 * @author alexandr
 */
public class ClassicSingleton {
    
    public static final ClassicSingleton instance = new ClassicSingleton();

//    
//    private ClassicSingleton () {}
//    
//    private static ClassicSingleton instance;
//
//      public static ClassicSingleton 
//    getInstance() {
//        
//            if(instance == null) {
//                instance = new ClassicSingleton();
//            }
//            return instance;
//        }
//    
//    
    public static void main(String[] args) throws CloneNotSupportedException {
        
        ClassicSingleton cl1 = ClassicSingleton.instance;
        ClassicSingleton cl2 = ClassicSingleton.instance;
        
        System.out.println(cl1.equals(cl2));
    }
}

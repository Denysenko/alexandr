package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class HatchbackAudi extends Audi {

      @Override public String 
    toString() {
        
            return "Created hatchback audi";
        }
}

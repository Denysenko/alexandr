package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public abstract class CarFactory {

      public static CarFactory 
    getFactory (BodyType bodyType) {

            switch (bodyType) {

                    case HATCHBACK:  return new HatchbackCarFactory(); 
                    
                    case SEDAN: return new SedanCarFactory();
                    
                    default: return null;
            }
        }
    
    public abstract Audi createAudi();
    public abstract Toyota createToyota();
    
}

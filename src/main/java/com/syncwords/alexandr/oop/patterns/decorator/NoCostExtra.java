package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public class NoCostExtra extends Extra {

    public NoCostExtra (Order order, String label) {
        
            super(order, 0, label);
        }

      @Override public double 
    getPrice() {
        
            return order.getPrice();
        }
}

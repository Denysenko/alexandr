package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public enum BodyType {
    
    SEDAN,
    HATCHBACK
}

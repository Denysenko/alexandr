package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public abstract class Car {

    @Override public abstract String toString();
}

package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public class Coffe implements Order {
    
    private String label;
    private double price;

      public 
    Coffe (String label, double price) {
        
            this.label = label;
            this.price = price;
        }

      @Override public double 
    getPrice() {
        
        return this.price;
    }

      @Override public String 
    getLabel() {

            return this.label + ". ";
        }
}

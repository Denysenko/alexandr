package com.syncwords.alexandr.oop.patterns.factory;

/**
 *
 * @author alexandr
 */
public class Main {
    
    public static void main(String[] args) {
        
        CoffeeHouse ch = new CoffeeHouse();
        
        Coffee americano = ch.create("Americano");
        Coffee latte = ch.create("Latte");

        americano.drink();
        latte.drink();
    }
}

package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public abstract class Toyota extends Car {

    @Override public abstract String  toString();
}

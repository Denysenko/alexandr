package com.syncwords.alexandr.oop.patterns.factory;

/**
 *
 * @author alexandr
 */
public class CoffeeHouse {
    
      public Coffee 
    create(String coffee) {
        
            switch(coffee) 
            {
                case "Americano" : return new Americano();
                
                case "Latte" : return new Latte();
                
                default: return null;
            }
        }
}

package com.syncwords.alexandr.oop.patterns.factory;

/**
 *
 * @author alexandr
 */
public interface Coffee {
    
    public void drink();
}

package com.syncwords.alexandr.oop.patterns.factory;

/**
 *
 * @author alexandr
 */
public class Latte implements Coffee {

      @Override public void 
    drink() {
        
            System.out.println("drink latte");
        }
    
}

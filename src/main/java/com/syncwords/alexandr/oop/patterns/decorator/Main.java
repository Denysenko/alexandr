package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public class Main {

    public static void main (String[] args) {
        
        Order pizza = new Pizza ("Base", 100);
        
        pizza = new DoubleExtra (pizza, 10, "Cheese");
        pizza = new RegularExtra (pizza, 8, "Chili");
        pizza = new RegularExtra (pizza, 5, "Pepperoni");
        pizza = new RegularExtra (pizza, 7, "Mozzarella");
        pizza = new DoubleExtra (pizza, 4, "Salyami");
        
        System.out.println ("Price: " + pizza.getPrice() + " y.e.\nComponents: " + pizza.getLabel());
        
        Order coffe = new Coffe ("Americano", 14);
        
        coffe = new DoubleExtra (coffe, 2, "Milk");
        coffe = new NoCostExtra (coffe, "Sugar");
        coffe = new RegularExtra (coffe, 5, "Banana syrup");

        System.out.println ("Price: " + coffe.getPrice() + " y.e.\nComponents: " + coffe.getLabel());
    }
}

package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class SedanAudi extends Audi {
    
      @Override public String 
    toString() {
        
            return "Created sedan audi";
        }
}

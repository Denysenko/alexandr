package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class SedanCarFactory extends CarFactory {
    
      @Override public Audi 
    createAudi() {
        
            return new SedanAudi();
        }

      @Override public Toyota 
    createToyota() {
    
            return new SedanToyota();
        }
}

package com.syncwords.alexandr.oop.patterns.prototype;

/**
 *
 * @author alexandr
 */
public class Circle implements Cloneable {
    
    private int radius;

      public 
    Circle(int radius) {
        
            this.radius = radius;
        }
    
      public int 
    getRadius() {
        
            return radius;
        }

      @Override public Circle 
    clone() {
        
            return new Circle (this.radius);
        }

      @Override public boolean 
    equals (Object obj) {
        
            if (this == obj) 
                return true;

            if (!(obj instanceof Circle))
                return false;

            return this.radius == ((Circle) obj).radius;
        }

      @Override public int 
    hashCode() {
        
            return super.hashCode();
        }
}

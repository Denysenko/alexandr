package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public class DoubleExtra extends Extra {
    
      public 
    DoubleExtra (Order order, double price, String label) {
        
            super(order, price, label);  
        }

      @Override public String 
    getLabel() {
        
            return order.getLabel() + "Double " + this.label + ". ";
        }

      @Override public double 
    getPrice() {
        
            return order.getPrice() + this.price * 2;
        }
}

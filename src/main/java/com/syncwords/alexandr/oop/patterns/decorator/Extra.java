package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public abstract class Extra implements Order {
    
    protected Order order;
    protected double price;
    protected String label;

      public 
    Extra (Order order, double price, String label) {
        
            this.order = order;
            this.price = price;
            this.label = label;
        }

      @Override public String 
    getLabel()  {
        
            return order.getLabel() + this.label + ". ";
        }
    

    @Override public abstract double getPrice();
    
    
}

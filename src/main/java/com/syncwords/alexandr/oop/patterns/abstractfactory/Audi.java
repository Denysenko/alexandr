package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public abstract class Audi extends Car {

     @Override public abstract String  toString();
}

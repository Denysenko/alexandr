package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class HatchbackCarFactory  extends CarFactory {

      @Override public Audi 
    createAudi() {
        
            return new HatchbackAudi();
        }

      @Override public Toyota 
    createToyota() {
    
            return new HatchbackToyota();
        }
}

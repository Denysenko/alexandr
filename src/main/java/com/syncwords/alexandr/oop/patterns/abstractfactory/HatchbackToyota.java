package com.syncwords.alexandr.oop.patterns.abstractfactory;

/**
 *
 * @author alexandr
 */
public class HatchbackToyota extends Toyota {
    
      @Override public String 
    toString() {
        
            return "Created hatchback toyota";
        }
}

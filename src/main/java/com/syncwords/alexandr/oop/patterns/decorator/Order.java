package com.syncwords.alexandr.oop.patterns.decorator;

/**
 *
 * @author alexandr
 */
public interface Order {
    
    public double getPrice();
    public String getLabel();
}

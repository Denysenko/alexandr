package com.syncwords.alexandr.oop.pattern.builder;

/**
 *
 * @author alexandr
 */
public class Main {
    
      public static void 
    main(String[] args) {
        
            SomeData someData = new SomeData.Builder()
                    .setTitle("Pattern Builder")
                    .setDescription("Some words about builder")
                    .setTag("patterns")
                    .setText("About pattern builder").build();
            
            SomeData notCompleteData = new SomeData.Builder()
                    .setTitle("only title")
                    .build();
            
            System.out.println(someData.toString());
            System.out.println();
            System.out.println(notCompleteData.toString());
        }
}

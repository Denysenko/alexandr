package com.syncwords.alexandr.oop.pattern.builder;

/**
 *
 * @author alexandr
 */
public class SomeData {
    
    private String title;
    private String description;
    private String text;
    private String tag;

    public static class Builder {
        
        private SomeData someData;

          public 
        Builder() {
        
                someData = new SomeData();
            }
        
          public Builder 
        setTitle(String title) {
            
                someData.title = title;
                return this;
            }
        
          public Builder 
        setDescription(String description) {
            
                someData.description = description;
                return this;
            }
                
          public Builder 
        setText(String text) {
            
                someData.text = text;
                return this;
            }
                        
          public Builder 
        setTag(String tag) {
            
                someData.tag = tag;
                return this;
            }

          public SomeData 
        build() {
            
                return someData;
            }
    }

    @Override
    public String toString() {
        return "Title : " + title + "\n"
                + "Description: " + description + "\n" 
                + "Tag: " + tag + "\n" 
                + "Text: " + text;
    }
    
    
}

package com.syncwords.alexandr.oop.companymanagement;

/**
 *
 * @author alexandr
 */
public interface IConsumer {
    
    void accept(String str);
    
}

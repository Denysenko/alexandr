package com.syncwords.alexandr.oop.companymanagement;

/**
 *
 * @author alexandr
 */
public interface ISupplier {
    
    String get();
}

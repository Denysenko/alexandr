package com.syncwords.alexandr.oop.shapes;

import java.awt.Color;

/**
 *
 * @author alexandr
 */
public abstract class DimensionalShape implements ShapeRenderer {
    
    protected Shape baseShape;
    protected float height;
    
      public float 
    getBaseArea() {
        
            return baseShape.getArea();
        };
    
      public float 
    getDimensional() {
        
            return getBaseArea() * height;
        }

}

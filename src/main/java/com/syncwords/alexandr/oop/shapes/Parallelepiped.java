package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Parallelepiped extends DimensionalShape {

      public 
    Parallelepiped(float sideBaseA, float sideBaseB, float height) {
        
            baseShape = new Rectangle(sideBaseA, sideBaseB);
            this.height = height;
        }
    
    //Cube
      public 
    Parallelepiped(float side) {
        
            baseShape = new Rectangle(side);
            this.height = side;
        }
    
      @Override public void 
    draw() {
            
            System.out.println("Draw parallelepiped");
        }

}

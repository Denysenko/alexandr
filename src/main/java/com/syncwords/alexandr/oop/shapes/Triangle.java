package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Triangle extends ShapeWithAngles {
    
      public 
    Triangle (float firstSide, float secondSide, float thirdSide) {
        
            sides[0] = firstSide;
            sides[1] = secondSide;
            sides[2] = thirdSide;
            perimeter = (sides[0] + sides[1] + sides[3]) / 2;
        }
    
      @Override public float 
    getArea() {
        
            return (float) (Math.sqrt (perimeter * (perimeter - sides[0]) * (perimeter - sides[1]) * (perimeter - sides[2])));
        }
    
      @Override public void 
    draw() {
        
            System.out.println("Draw triangle");
        }
}

package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Cylinder extends DimensionalShape {

      public 
    Cylinder(float radius, float height) {
        
            baseShape = new Circle(radius);
            this.height = height;
        }
    
      @Override public void 
    draw() {
        
            System.out.println("Draw Cylinder");
        }
}

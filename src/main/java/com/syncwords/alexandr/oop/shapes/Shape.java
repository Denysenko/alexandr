package com.syncwords.alexandr.oop.shapes;

import java.awt.Color;

/**
 *
 * @author alexandr
 */

public abstract class Shape implements ShapeRenderer{
    
    protected float perimeter;
    
    public abstract float getArea();

}

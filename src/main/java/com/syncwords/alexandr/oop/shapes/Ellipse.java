package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Ellipse extends ShapeWithoutAngles{

    private float height;
    private float wigth;

      public 
    Ellipse (float wigth, float height) {
        
            this.height = height;
            this.wigth = wigth;
        }

      @Override public float 
    getArea() {
        
            return (float) (Math.PI * height * wigth / 4);
        }
    
      @Override public void 
    draw() {
        
            System.out.println("Draw ellipse");
        }
}

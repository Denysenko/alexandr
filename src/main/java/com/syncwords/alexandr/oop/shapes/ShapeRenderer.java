package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public interface ShapeRenderer {
    
    void draw();
    
}

package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Circle extends ShapeWithoutAngles {
    
      public 
    Circle (float radius) {
        
            this.radius = radius;
            perimeter = (float) (2 * Math.PI * radius);
        }
    
      public float
    getRadius () {
        
            return radius;
        }
    
      @Override public float 
    getArea () {
    
            return (float) (Math.PI * Math.pow(radius, 2));
        }   

      @Override public void 
    draw() {
        
            System.out.println("Draw cirlce");
        }
}

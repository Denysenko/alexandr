package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Rectangle extends ShapeWithAngles{

      public 
    Rectangle(float sideA, float sideB) {
    
            sides[0] = sideA;
            sides[1] = sideB;

            perimeter = sides[0] * 2 + sides[1] * 2;
        }
    
    //realize for square
          public 
    Rectangle(float side) {
    
            sides[0] = sides[1] = side;

            perimeter = sides[0] * 4;
        }
    
      @Override public float 
    getArea() {
        
            return sides[0] * sides[1];
        }
    
      @Override public void 
    draw() {
        
            System.out.println("Draw rectangle");
        }
    
    
}

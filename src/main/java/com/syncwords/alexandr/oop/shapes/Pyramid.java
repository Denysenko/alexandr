package com.syncwords.alexandr.oop.shapes;

/**
 *
 * @author alexandr
 */
public class Pyramid extends DimensionalShape {

      public 
    Pyramid(float firstSide, float secondSide, float thirdSide, float height) {
        
            baseShape = new Triangle(firstSide, secondSide, thirdSide);
            this.height = height;
        }
    
      @Override public float 
    getDimensional () {
        
            return (float) (getBaseArea() * height / 3);
        }
    
      @Override public void 
    draw() {
        
            System.out.println("Draw pyramid");
        }
}

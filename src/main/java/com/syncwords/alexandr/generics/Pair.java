package com.syncwords.alexandr.generics;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexandr
 * @param <KeyType>
 * @param <ValueType>
 */
public class Pair <KeyType, ValueType>  {
    
    Map<KeyType, ValueType> pairs = new HashMap<>();

      public void 
    save (KeyType key, ValueType value) {
        
            pairs.put (key, value);
        }
    
      public ValueType 
    getValueByKey (KeyType key) {
        
            return pairs.get (key);
        }
}

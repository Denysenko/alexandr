package com.syncwords.alexandr.multithreading.httpparser;

import com.syncwords.alexandr.multithreading.incrementation.IncrementSynchronize;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author alexandr
 */
public class DownloadHTML extends Thread {

    private String url;
    private IncrementSynchronize incrementSynchronize;
    
    Document doc; 
    
      public 
    DownloadHTML (String url, IncrementSynchronize incrementSynchronize) {
        
            this.url = url;
            this.incrementSynchronize = incrementSynchronize;
        }

      @Override public void 
    run() {
        
            try 
            {
                System.out.println (getName() + ". Downloading...");
                doc = Jsoup.connect (url).get();

                String page = doc.html();
                
                String path = "parsed_site";
                
                File directory = new File (String.valueOf (path));
                
                if (!directory.exists()) {
                    
                    directory.mkdir();
                    System.out.println ("Folder " + directory.getPath() + " is created");
                }

                try (FileOutputStream ostream = new FileOutputStream (directory.getPath() + "/" + getName() + ".html")) {

                    byte[] bytes = page.getBytes();

                    ostream.write (bytes);

                    System.out.println (incrementSynchronize.getNextValue1() + " finished downloading " + getName() + ", url: " + url);
                } 
            } 
            catch (IllegalArgumentException ex) 
            {
                System.out.println ("Bad input url..");
            }
            catch (IOException ex) 
            {
                System.out.println ("Problem with io stream. " + getName()  + " " + url);
            }
        }
}
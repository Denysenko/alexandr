package com.syncwords.alexandr.multithreading.httpparser;

import com.syncwords.alexandr.multithreading.incrementation.IncrementSynchronize;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author alexandr
 */
public class HTMLParser {
    
    static Document doc;
    static String title;
    static Elements links;
    
    
    /***
     * Use HTMLParser.parse("Url") for parse and save website.
     * 
     * @param url example: {@link www.google.com} or {@link www.geeksforgeeks.org}  
     */
      static void 
    parse (String url) {

            try 
            {
                System.out.println ("Main Thread start");

                doc = Jsoup.connect (url).get();
                title = doc.title();

                links = doc.select ("a[href]");

                ExecutorService pool = Executors.newFixedThreadPool (links.size());
                
                IncrementSynchronize incrementSynchronize =  new IncrementSynchronize();

                for (Element link : links) {

                    pool.execute (new DownloadHTML (link.attr ("abs:href"), incrementSynchronize));
                }

                pool.shutdown();

                System.out.println ("Main Thread is finished");
            } 
            catch (IllegalArgumentException ex) 
            {
                System.out.println ("Bad url");
            }
            catch (IOException ex) 
            {
                System.out.println ("Problem with io stream");
            }
        }
}

package com.syncwords.alexandr.multithreading.ChickenEggThreads;

import java.util.Random;

/**
 *
 * @author alexandr
 */
public class EggRunnable implements Runnable  {

      @Override public void 
    run() {
        
            for (int i = 0; i < 3; i++) {

                try 
                {
                    int randomTime = (new Random().nextInt (10)  + 1) * 1000;
    
                    System.out.println (i + " egg + " + randomTime / 1000 + " sec");
                    
                    Thread.sleep (randomTime);
                } 
                catch (InterruptedException ex) 
                {
                    System.out.println ("Interrupted exception");
                }            
            }
            System.out.println ("Egg thread is done");
        }
}

package com.syncwords.alexandr.multithreading.ChickenEggThreads;

/**
 *
 * @author alexandr
 */
public class ThreadRaceDemo {
    
    
    /***
     *
     * Who appear first? Chicken or Egg
     * 
     * @param args 
     *
     ***/
      public static void 
    main (String[] args) {
        
            ChickenThread chicken = new ChickenThread (); 
            
            Thread egg = new Thread (new EggRunnable());

            chicken.start();
            egg.start();
            
            try 
            {
                chicken.join();
                if (egg.isAlive()) 
                {
                    egg.join();
                    System.out.println ("First appear egg");
                }
                else
                {
                    System.out.println ("First appear chicken");
                }

            } 
            catch (InterruptedException ex) 
            { }
    }
}

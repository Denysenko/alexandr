package com.syncwords.alexandr.multithreading.incrementation;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author alexandr
 */
public class IncrementSynchronize {
    
    private final AtomicInteger value = new AtomicInteger(0);
    
    /***
     * 
     * Synchronized (safe) method to increment using AtomicInteger and compareAndSet(old, new) method
     * 
     * @return (Integer) incremented value
     * 
     ***/
    
       public int 
    getNextValue1() {

            while (true) {
                
                int oldvalue = value.get();
                int newValue = oldvalue + 1;

                if (value.compareAndSet (oldvalue, newValue)) {
                    
                    return value.get();
                } 
                else 
                    System.out.println ("thread collapse, try more..");
            }
        }
}

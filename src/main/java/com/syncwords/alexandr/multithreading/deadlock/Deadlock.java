package com.syncwords.alexandr.multithreading.deadlock;

/**
 *
 * @author alexandr
 */
public class Deadlock {
    
    /*** 
     * 
     * 
     * This method try to catch multithread deadlock. And show how to avoid it.
     * 
     * @param args 
     */
      public static void 
    main (String[] args) {
        
            Entity ent1 = new Entity ("some text 1");
            Entity ent2 = new Entity ("some text 2");

            Thread thread1 = new Thread() {

                  @Override public void 
                run() {
                        System.out.println (getName() + " start");

                        swapTextInObjects (ent1, ent2);

                        System.out.println (getName() + " finished.");
                    } 
            };

            Thread thread2 = new Thread() {

                  @Override public void 
                run() {
                        System.out.println (getName() + " start");

                        swapTextInObjects (ent2, ent1);

                        System.out.println (getName() + " finished.");
                    } 
            };

            thread1.start();
            thread2.start();
        }
    
      public static void 
    swapTextInObjects (Entity en1, Entity en2) {
        
            for (int i = 0; i < 100; i++) {

                while (true) {

                    if (en1.lock.tryLock()) {

                        if (en2.lock.tryLock()) 
                        {
                            try 
                            {
                                en1.setSomeText (en2.getSomeText());
                                en2.setSomeText (en1.getSomeText());
                                
                                break;
                            }
                            finally 
                            {
                                en1.lock.unlock();
                                en2.lock.unlock();  
                            }
                        } 
                        else 
                            en1.lock.unlock();
                    }
                }
            }
        }
}

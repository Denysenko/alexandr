package com.syncwords.alexandr.multithreading.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author alexandr
 */
public class Entity {
    
    private String someText;
    Lock lock;

      public 
    Entity (String someText) {
        
            this.someText = someText;
            lock = new ReentrantLock();
        }
    
      public String 
    getSomeText() {
        
            return someText;
        }

      public void 
    setSomeText (String someText) {
        
            this.someText = someText;
        }
}
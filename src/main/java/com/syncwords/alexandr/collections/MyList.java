package com.syncwords.alexandr.collections;

/**
 *
 * @author alexandr
 * @param <T>
 */
public interface MyList<T> extends MyIterable<T> {
    
    static final int DEFAULT_START_ARRAY_POSITION = -1;
    static final int DEFAULT_START_ARRAY_LENGTH = 10;
    static final double DEFAULT_INCREASED_LIST_COEF = 1.5;
    
    void add(T obj);
    
    T get(int index);
    
    int size();
    
    void remove(int index);
    
    void validateCapacity();

}

package com.syncwords.alexandr.collections;

/**
 *
 * @author alexandr
 * @param <T>
 */
public interface MyIterator<T> {
    
    static final int DEFAULT_POSITION = -1;
    
    boolean hasNext();
    
    T next();
    
    void remove();
}

package com.syncwords.alexandr.collections;

/**
 *
 * @author alexandr
 * @param <T>
 */
public class MyArrayList<T> implements MyList<T> {

    T[] list;
    
    int posLastElement;

      public 
    MyArrayList() {

            list = (T[]) new Object[DEFAULT_START_ARRAY_LENGTH];
            posLastElement = DEFAULT_START_ARRAY_POSITION;
        }
    
      @Override public void 
    add (T obj) {
        
            validateCapacity();
            list[++posLastElement] = obj;
        }

      @Override public T 
    get (int index) {
        
            if (index <= posLastElement)
                return list[index];
            else 
                return null; // throw new IndexOutOfBoundException();
        }

      @Override public int 
    size() {

            return posLastElement + 1;
        }

      @Override public void 
    remove (int index) {

            boolean isRemoved = false;

            for (int i = index; i < posLastElement; i++) {
                
                list[i] = list[i+1];

                isRemoved = true;
            }
            
            if (isRemoved || index == posLastElement) 
            {
                list[posLastElement] = null;
                --posLastElement;
            } 
            // else throw new IndexOutOfBoundException();
        }

      @Override public void 
    validateCapacity() {
        
            if (!(posLastElement < list.length - 1)) 
            {
                T[] tempList = list;

                list = (T[]) new Object[(int)(list.length * DEFAULT_INCREASED_LIST_COEF)];

                System.arraycopy (tempList, 0, list, 0, posLastElement + 1);
            }
        }

      @Override public MyIterator<T> 
    iterator() {
        
            return new MyItr();
        }

    private class MyItr implements MyIterator<T> {

        int position = DEFAULT_POSITION;

          @Override public boolean 
        hasNext() {

                return posLastElement != position;
            }

          @Override public T 
        next() {
            
                return hasNext() ? list[++position] : null;
            }

          @Override public void 
        remove() {
            
                boolean isRemoved = false;

                for (int i = position; i < posLastElement; i++) {

                    list[i] = list[i+1];

                    isRemoved = true;
                }

                if (isRemoved || position == posLastElement) 
                {
                    list[posLastElement] = null;
                    --posLastElement;
                }
                // else throw new IndexOutOfBoundException();
            }
    }
}

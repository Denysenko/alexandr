package com.syncwords.alexandr.collections;

/**
 *
 * @author alexandr
 * @param <T>
 */
public interface MyIterable<T> {
    
    MyIterator<T> iterator();
}

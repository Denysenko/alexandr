package com.syncwords.alexandr.hashcode;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author alexandr
 */
public class Employee {
    
    private int id;
    private String name;
    private int salary;
    private int age;
    private Date dateOfJoining;

      public 
    Employee (int id, String name, int salary, int age, Date dateOfJoining) {
        
            this.id = id;
            this.name = name;
            this.salary = salary;
            this.age = age;
            this.dateOfJoining = dateOfJoining;
        }
    
      @Override public boolean 
    equals (Object obj) {
        if (this == obj) 
            return true;
        
        if (!(obj instanceof Employee))
            return false;
        
        return this.id == ((Employee) obj).id
            && Objects.equals(this.name, ((Employee) obj).name)
            && this.salary == ((Employee) obj).salary
            && this.age == ((Employee) obj).age
            && Objects.equals (this.dateOfJoining, ((Employee) obj).dateOfJoining);
    
        }
    
      @Override public int 
    hashCode() {
        
            return Objects.hash (this.id, this.name, this.salary, this.age, this.dateOfJoining);
        }
}

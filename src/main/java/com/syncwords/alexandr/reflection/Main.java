package com.syncwords.alexandr.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 *
 * @author alexandr
 */
public class Main {
    
      public static void 
    main (String[] args) {
        
            int passed = 0;
            int notPassed = 0;
            int ignored = 0;

            Class<CalculatorTest> calcTest = CalculatorTest.class;

            for (Method method : calcTest.getDeclaredMethods()) {

                System.out.print ("Method: " + method.getName());
                
                if (method.isAnnotationPresent (MyTest.class)) {

                    Annotation annotation = method.getAnnotation (MyTest.class);
                    
                    if (((MyTest) annotation).enabled())
                    {
                        try 
                        {
                            method.invoke (calcTest.newInstance());
                            passed++;
                            System.out.println (" - passed");
                        } 
                        catch (Exception ex) 
                        {
                            notPassed++;
                            System.out.print (" - Exception: " + ex);
                            System.out.println (" - not passed");
                        } 
                    } 
                    else 
                    {
                        ignored++;
                        System.out.println (" - ignored");
                    }
                }
            }
            
            System.out.println ("passed: " + passed + "\n"
                    + "not passed: " + notPassed + "\n"
                    + "ignored: " + ignored + "\n"
            );
        }
}

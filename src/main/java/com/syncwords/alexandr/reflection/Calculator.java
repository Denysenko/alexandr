package com.syncwords.alexandr.reflection;

/**
 *
 * @author alexandr
 */
public class Calculator implements Calculation {

    /***
     * 
     * This method was created only for understanding how to work {@code method.getDeclaratedAnnotation()}
     * 
     * @deprecated
     * @see #addition(double, double) 
     */
      @MyTest @Deprecated private double 
    additionTest (double a, double b) {
        
            return a + b;
        }
    
    /***
     * 
     * This method was created only for understanding how to work {@code method.getDeclaratedAnnotation()}
     * 
     * @deprecated
     * @see #addition(double, double) 
     */
      @Deprecated public double 
    additionDeprecated (double a, double b) {
        
            return a + b;
        }
    
      @Override public double 
    addition (double a, double b) {
        
            return a + b;
        }

      @Override public double 
    subtraction (double a, double b) {
        
            return a - b;
        }

      @Override public double 
    multiplication (double a, double b) {
        
            return a * b;
        }

      @Override public double 
    division (double a, double b) {
        
            if (b != 0)
                return a / b;
            else
                throw new ArithmeticException ("Division by zero");
        }
}

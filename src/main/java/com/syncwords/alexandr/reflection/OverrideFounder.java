package com.syncwords.alexandr.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 *
 * @author alexandr
 */
public class OverrideFounder {
    
      public void 
    foundOverrideAnnotation (Class<?> clazz) {
        
            Class<?> object = clazz;

            for (Method method : object.getDeclaredMethods()) {

                System.out.println (method.toString());

                for (Annotation annotation : method.getDeclaredAnnotations()) {

                    if (annotation != null) {

                        System.out.println (annotation.toString());
                    }
                }   
            }

            for (Type type : object.getGenericInterfaces()) {

                System.out.println (type.toString());

            }
        }

      public static void 
    main (String[] args) {
        
            OverrideFounder ovFounder = new OverrideFounder();

            ovFounder.foundOverrideAnnotation (Calculator.class);   
        }
}

package com.syncwords.alexandr.reflection;

/**
 *
 * @author alexandr
 */
public class CalculatorTest {
    
    Calculator calc = new Calculator();

      @MyTest (enabled = true) public void 
    testAddition() {
    
            calc.division (2.4, 3.6); // passed
        }
    
      @MyTest (enabled = true) public void 
    testSubtraction() {
    
            calc.division (2.4, 3.6); // passed
        }
    
      @MyTest (enabled = true) public void 
    testMultiplication() {
    
            calc.division (2.4, 3.6); // passed
        }
    
      @MyTest (enabled = true) public void 
    testDivision() {
    
            calc.division (2.4, 3.6); // passed
        }
    
      @MyTest (enabled = true) public void 
    testDivisionByZero() {
        
            calc.division (5, 0); // return Exception
        }
    
      @MyTest (enabled = false) public void 
    testAddition1() {
    
            calc.division (2.4, 3.6); // not passed
        }
    
      @MyTest (enabled = false) public void 
    testAddition2() {
    
            calc.division (2.4, 3.6); // not passed
        }
   
}

package com.syncwords.alexandr.exceptions;

/**
 *
 * @author alexandr
 */
public class ArchiveManager {

    final static String[] LOGIN_ARCHIVE = {"Alex", "Bob", "Mark"};
    
      public static String
    getFreeLoginFromArchive (String login) throws ArchiveLoginNotFoundException {
        
        for (String n : LOGIN_ARCHIVE) {
            
            if (n.equals(login))
                throw new ArchiveLoginNotFoundException();
        }
        
        return "Login is available";
    }
}

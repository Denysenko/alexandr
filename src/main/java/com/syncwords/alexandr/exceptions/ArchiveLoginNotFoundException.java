package com.syncwords.alexandr.exceptions;

import javax.naming.NameNotFoundException;

/**
 *
 * @author alexandr
 */
public class ArchiveLoginNotFoundException extends NameNotFoundException{

      public String 
    getExceptionMessage() {

            return "ArchiveNameNotFoundException: This login is not available";
        }
}

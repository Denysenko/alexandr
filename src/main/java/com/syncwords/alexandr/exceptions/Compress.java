package com.syncwords.alexandr.exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 *
 * @author alexandr
 */
public class Compress {

      public static void 
    toZip (String archiveName, String pathFolder) throws IOException {

            File files = new File (pathFolder);
            
            if (files.isDirectory())
            {
                try (ZipOutputStream zipOut = new ZipOutputStream (new FileOutputStream (archiveName))) {
                    openDirectory (files, zipOut);
                }
            }
            else 
                System.out.println ("Bad directory path.");
        }
    
      private static void 
    openDirectory (File dir, ZipOutputStream zipOut) throws IOException {

            for (File f : dir.listFiles()) {

                if (f.isDirectory())
                    openDirectory (f, zipOut);
                else 
                {
                    String path = f.getPath();
                    
                    zipOut.putNextEntry (new ZipEntry (path));
                    
                    try (FileInputStream fileInStream = new FileInputStream (f)) {
                        writeFile (fileInStream, zipOut);
                    }
                }
            }
        }

      private static void 
    writeFile (InputStream in, OutputStream zipOut) throws IOException {

            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read (buffer)) >= 0)
                zipOut.write (buffer, 0, len);
            in.close();
        }
}

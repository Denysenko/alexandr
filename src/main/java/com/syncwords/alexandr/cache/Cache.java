package com.syncwords.alexandr.cache;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author alexandr
 */
public class Cache {

    private final Map<String, CacheEntry> cacheMap = new HashMap<> ();
    
      public void 
    save (String key, Object object) {
        
            cacheMap.put (key, new CacheEntry (object));
        }
    
      public Object 
    getObject (String key) {
        
            return cacheMap.get (key).getValue();
        }
    
      public String
    getFullInfoOfObjectByKey(String key) {
        
        return "Object: " + cacheMap.get (key).getValue()
                + ", time inserted: " + cacheMap.get (key).getTimeInsert();
    }
    
      public void 
    remove (String key) {
        
            cacheMap.remove (key);
        }
    
    static class CacheEntry {

        private final Date timeInserted;
        private final Object value;

        CacheEntry (Object value) {

                this.value = value;
                timeInserted = new Date();
            }

          Object 
        getValue() {

                return value;
            }

          Date 
        getTimeInsert() {

                return timeInserted;
            }
    }
}

package com.syncwords.alexandr.enums;

/**
 *
 * @author alexandr
 */
enum Planet {
    
    NEPTUNE (8),
    URANUS (7),
    SATURN (6),
    JUPITER (5),
    MARS (4),
    EARTH (3),
    VENUS (2),
    MERCURY (1);
    
    int positionToTheSun;
    
    Planet (int positionToTheSun) {
        
            this.positionToTheSun = positionToTheSun;
        }
    
      int 
    getPlanetPositionToTheSun() {
        
            return this.positionToTheSun;
        }
    
      String 
    getPlanetName() {
        
            return this.name();
        }
}

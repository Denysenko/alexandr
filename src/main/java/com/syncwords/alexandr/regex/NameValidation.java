package com.syncwords.alexandr.regex;

/**
 *
 * @author alexandr
 */
public class NameValidation {
    
      public static boolean 
    validate(String name) {
        
            String pattern = "^[a-z0-9_-]{3,15}$";

            return name.matches(pattern);
        }
}

package com.syncwords.alexandr.anonymousclasses;

import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author alexandr
 */
public class Employee implements Comparable<Employee> {
    
    private int id;
    private String name;
    private int salary;
    private int age;
    private Date dateOfJoining;

    public Employee(int id, String name, int salary, int age, Date dateOfJoining) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.age = age;
        this.dateOfJoining = dateOfJoining;

    }

      @Override public int 
    compareTo (Employee o) {
        
            return this.id > o.id ? 1 : this.id < o.id ? -1 : 0; 
        }
    
    static Comparator<Employee> byId = new Comparator<Employee>() {
        
        @Override
        public int compare (Employee o1, Employee o2) {
            
            return o1.id > o2.id ? 1 : o1.id < o2.id ? -1 : 0;
        }
    };
    
    static Comparator<Employee> byName = new Comparator<Employee>() {
        
        @Override
        public int compare (Employee o1, Employee o2) {
            
            return o1.name.compareTo (o2.name);
        }
    };
    
    static Comparator<Employee> byAge = new Comparator<Employee>() {
        
        @Override
        public int compare (Employee o1, Employee o2) {
            
            return o1.age > o2.age ? 1 : o1.age < o2.age ? -1 : 0;
        }
    };
    
    static Comparator<Employee> bySalary = new Comparator<Employee>() {
        
        @Override
        public int compare (Employee o1, Employee o2) {
            
            return o1.salary > o2.salary ? 1 : o1.salary < o2.salary ? -1 : 0;
        }
    };
    
    static Comparator<Employee> byDate = new Comparator<Employee>() {
        
        @Override
        public int compare (Employee o1, Employee o2) {
            
            return o1.dateOfJoining.compareTo (o2.dateOfJoining);
        }
    };
}

package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class FinalValues {
    
    private final int FIRST = 5;
    private final int SECOND = 10;
    
      public int 
    getFirst () {
        
        return FIRST;
    }
    
       public int 
    getSecond () {
        
        return SECOND;
    }
}

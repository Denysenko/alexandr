package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class SwitchCase {
    
      public static String 
    switchCaseDefinition (int a) {
        
        final int FIVE = 5;
        final int TEN = 10;
        
        switch (a) 
        {
            case FIVE: return "5";
            case TEN: return "10";
            default: return "Default number";
        }
    }
}

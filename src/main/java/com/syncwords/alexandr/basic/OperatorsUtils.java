package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class OperatorsUtils {
    
      public static float 
    getMinValue (float arg1, float arg2) {
        
//        return arg1 < arg2 ? arg1 : arg2;
        return Math.min (arg1, arg2);
    }
    
      public static int 
    getAbsoluteValue (int value) {
        
        return value < 0 ? -value : value;
    }
    
      public static boolean 
    invertBoolean (boolean value) {
            
        return !value; // return value ? false : true;
    } 
    
      public static boolean 
    isContainString (String word) {
        
        final String line = "Hi, my name is Alexandr!";
        return line.contains (word) ? true : false;
    }
    
      public static double
    customLog (double a, double b) {
        
        return Math.log (b) / Math.log (a);
    }
}

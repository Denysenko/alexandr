package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class ConditionalStatement {
    
      public static String 
    definePosOrNegNumber (int a) {

        if (a > 0) 
        {
            return "The number is positive: " + a;
        } 
        else if (a < 0) 
        {
            return "The number is negative: " + a;
        }
        else 
            return "Zero number";
    }
}

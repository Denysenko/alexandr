package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class MatrixOperations {
    
      public static int[][] 
    matrixMultiplication (int[][] matrixA, int[][] matrixB) {        

        int aCols = matrixA.length;
        int aRows = matrixA[0].length;
        int bCols = matrixB.length;
        int bRows = matrixB[0].length;
        
        assert (aRows == bCols) : "Multiply impossible";
        
        int[][] finalMatrix = new int[aCols][bRows];
        
        for (int i = 0; i < aCols; i++) 
        {
            for (int j = 0; j < bRows; j++) 
            {          
                for (int n = 0; n < bCols; n++)
                {
                    finalMatrix[i][j] += matrixA[i][n] * matrixB[n][j];
                }
            }   
        }
        
        return finalMatrix;
    }
    
      public static void 
    printMatrix (int[][] matrix) 
    {
        if (matrix != null) 
        {
            for (int i = 0; i < matrix.length; i++)
            {
                for (int j = 0; j < matrix[0].length; j++)
                {
                    System.out.print (matrix[i][j] + " ");
                }
                System.out.println();
            }
        }
        else 
        {
            System.out.println ("Output matrix is null");
        }
    }
}

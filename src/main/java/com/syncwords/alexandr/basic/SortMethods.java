package com.syncwords.alexandr.basic;

/**
 *
 * @author alexandr
 */
public class SortMethods {
    
      public static int[] 
    bubbleSort(int[] mass)
    {
        boolean isSorted = false;
                
        while (!isSorted) 
        {
            isSorted = true;
            
            for (int i = 0; i < mass.length - 1; i++) 
            {
                if (mass[i] > mass[i + 1])
                {
                    int temp;
                    temp = mass[i];
                    mass[i] = mass[i + 1];
                    mass[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
        return mass;
    }
}
